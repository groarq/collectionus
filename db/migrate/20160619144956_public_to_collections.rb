class PublicToCollections < ActiveRecord::Migration
  def change
    add_column :collections, :public, :boolean, default: true
  end
end

