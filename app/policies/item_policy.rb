class ItemPolicy < ApplicationPolicy

  def create?
    record_user && @user.collections.include?(@record.collection)
  end

  def update?
    record_user && @user.collections.include?(@record.collection)
  end

  def destroy?
    # return true if @user.admin?
    record_user
  end

  def record_user
    @user == @record.collection.user
  end
end
