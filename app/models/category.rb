class Category < ActiveRecord::Base

  # validataions
  validates :name, presence: true
  validates :name, length: { maximum: 50 }
end
