class Comment < ActiveRecord::Base

  include ActsAsCommentable::Comment

  # relations
  belongs_to :commentable, polymorphic: true
  belongs_to :user

  # validations
  validates :comment, presence: true

  scope :by_created_time, -> { order('created_at ASC') }

  # NOTE: install the acts_as_votable plugin if you
  # want user to vote on the quality of comments.
  #acts_as_voteable
end
