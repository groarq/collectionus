class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|

      t.references :collection, index: true
      t.string :year
      t.string :tags
      t.string :description
      t.string :image
      t.integer :condition

      t.timestamps null: false
    end
  end
end
