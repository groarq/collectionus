class Offer < ActiveRecord::Base
  belongs_to :seller, foreign_key: :seller_id, class_name: User
  belongs_to :buyer, foreign_key: :buyer_id, class_name: User
  belongs_to :item
  acts_as_commentable

  enum status: [:unread, :read, :accepted, :rejected]

  validates :seller, :buyer, :item, presence: true

  scope :unread, -> { where(status: 'unread') }

  def finished?
    status == 'accepted' || status == 'rejected'
  end
end
