class CollectionPolicy < ApplicationPolicy

  def show?
    record_user || @record.public
  end

  def create?
    record_user
  end

  def update?
    record_user
  end

  def destroy?
    # return true if @user.admin?
    record_user
  end

  def record_user
    @user == @record.user
  end

  def public?
    update?
  end

  def hide?
    update?
  end
end
