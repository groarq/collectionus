class Collection < ActiveRecord::Base

  # relations
	has_many :items, dependent: :destroy
	belongs_to :user

  # validations
  validates :name, :user_id, presence: true
  validates :name, length: { maximum: 50 }

  #scopes
  scope :not_hide, -> { where(public: true) }
end
