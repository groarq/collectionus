class CreateOffer < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.integer :buyer_id
      t.integer :seller_id
      t.integer :status, default: 0
      t.integer :item_id
      t.text    :description
    end
  end
end
