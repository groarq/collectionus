class CollectionsController < ApplicationController
  before_action :set_collection, only: [:show, :edit, :update, :destroy, :public, :hide]

  # GET /collections
  # GET /collections.json
  def index
    if params[:user_id]
      user = User.find(params[:user_id])
      if user == current_user
        @collections = user.collections.order(updated_at: :asc)
      else
        @collections = user.collections.not_hide.order(updated_at: :asc)
      end
    else
      @collections = Collection.all.not_hide.order(updated_at: :asc)
    end
  end

  # GET /collections/1
  # GET /collections/1.json
  def show
    authorize @collection
    @items = @collection.items
  end

  # GET /collections/new
  def new
    @collection = Collection.new
  end

  # GET /collections/1/edit
  def edit
    authorize @collection
  end

  # POST /collections
  # POST /collections.json
  def create
    @collection = Collection.new(collection_params)
    @collection.user_id = current_user.id
    respond_to do |format|
      if @collection.save
        format.html { redirect_to @collection, notice: 'Collection was successfully created.' }
        format.json { render :show, status: :created, location: @collection }
      else
        format.html { render :new }
        format.json { render json: @collection.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /collections/1
  # PATCH/PUT /collections/1.json
  def update
    authorize @collection
    respond_to do |format|
      if @collection.update(collection_params)
        format.html { redirect_to @collection, notice: 'Collection was successfully updated.' }
        format.json { render :show, status: :ok, location: @collection }
      else
        format.html { render :edit }
        format.json { render json: @collection.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /collections/1
  # DELETE /collections/1.json
  def destroy
    authorize @collection
    @collection.destroy
    respond_to do |format|
      format.html { redirect_to collections_url, notice: 'Collection was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def public
    authorize @collection
    @collection.update(public: true) unless @collection.public
    redirect_to :back, notice: "Colelction: #{@collection.name} is now public for other users"
  end

  def hide
    authorize @collection
    @collection.update(public: false) if @collection.public
    redirect_to :back, notice: "Collection: #{@collection.name} is now hidden form other users"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_collection
      @collection = Collection.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def collection_params
      params.require(:collection).permit(:name, :description, :contains, :tags, :image)
    end
end
