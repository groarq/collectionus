class ApplicationController < ActionController::Base
  include Pundit

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  protect_from_forgery

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  before_action :authenticate_user!
  before_action :banned?
  before_action :unread_messages
  before_action :configure_permitted_parameters, if: :devise_controller?


  def configure_permitted_parameters
    added_attrs = [:nickname, :email, :password, :password_confirmation, :remember_me, :first_name,
                   :last_name, :address, :city, :country]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
  end

  private

  def unread_messages
    if current_user
      @unread = current_user.sell_offers.unread.count
    else
      0
    end
  end

  def user_not_authorized
    flash[:alert] = 'You are not authorized to perform this action.'
    redirect_to(root_path)
  end

  def banned?
    if current_user.present? and current_user.banned?
      sign_out current_user
      flash.clear
      flash[:error] = 'This account has been suspended....'
      root_path
    end
  end

end
