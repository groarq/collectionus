class OffersController < ApplicationController

  def index
    @to_buy = current_user.buy_offers
    @to_sell = current_user.sell_offers
  end

  def show
    @offer = Offer.find(params[:id])
    authorize @offer
    @offer.update(status: 'read') if @offer.seller == current_user && @offer.status == 'unread'
    @comments = @offer.comments
    @comment = Comment.new
  end

  def accept
    @offer = Offer.find(params[:id])
    authorize @offer
    @offer.update(status: 'accepted')
    redirect_to offers_path, notice: 'Accepted'
  end

  def reject
    @offer = Offer.find(params[:id])
    authorize @offer
    @offer.update(status: 'rejected')
    redirect_to offers_path, notice: 'Rejected'
  end

  def create
    offer = Offer.new(offer_params)
    offer.seller = Item.find(offer.item_id).user
    offer.buyer = current_user
    if offer.save
      redirect_to collection_path(offer.item.collection_id), notice: 'Offer created.'
    else
      redirect_to item_path(offer.item), notice: 'Error, try again later.'
    end
  end

  protected

  def offer_params
    params.require(:offer).permit(:item_id, :description)
  end

end
