class User < ActiveRecord::Base

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :collections
  has_many :comments, dependent: :destroy

  has_many :buy_offers, foreign_key: :buyer_id, class_name: Offer
  has_many :sell_offers, foreign_key: :seller_id, class_name: Offer

  validates :nickname, presence: true
  validates :nickname, uniqueness: true

  def full_name
    first_name.to_s + ' ' + last_name.to_s
  end

  def full_address
    address.to_s + ' ' + city.to_s + ' ' + country.to_s
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_h).where(["lower(nickname) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    elsif conditions.has_key?(:nickname) || conditions.has_key?(:email)
      where(conditions.to_h).first
    end
  end

  def login=(login)
    @login = login
  end

  def login
    @login || self.nickname || self.email
  end
end
