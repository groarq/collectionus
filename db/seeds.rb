# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user = User.create(email: 'user@user.pl', password: 'password', confirmed_at: DateTime.now, admin: true, first_name: 'user', last_name: 'user', nickname: 'user')
User.create(email: 'banned@banned.com', password: 'banned123', confirmed_at: DateTime.now, banned: true, first_name: 'banned', last_name: 'banned', nickname: 'banned')

collection = Collection.create(name: 'Kapsle', user_id: user.id)
Item.create(collection_id: collection.id, description: 'Fixxxer', year: 1999, condition: :mint )
Item.create(collection_id: collection.id, description: 'Lśnienie', year: 2005, condition: :near_mint )
Item.create(collection_id: collection.id, description: 'Flaszen', year: 2006, condition: :very_good )
Item.create(collection_id: collection.id, description: 'Striker', year: 2010, condition: :good )
Item.create(collection_id: collection.id, description: 'Raciborski porter', year: 2015, condition: :good )
Item.create(collection_id: collection.id, description: 'Pierwsza Pomoc', year: 2016, condition: :used )
Item.create(collection_id: collection.id, description: 'Imperium Atakuje', year: 2014, condition: :damaged )
