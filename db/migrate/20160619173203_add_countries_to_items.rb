class AddCountriesToItems < ActiveRecord::Migration
  def change
  	change_table :items do |t|
        t.string :country
    end    
  end
end
