class Item < ActiveRecord::Base

  # relations
	belongs_to :collection
  has_one :user, through: :collection
  has_many :offers, dependent: :destroy

	# For Paperclip
	has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
   	validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

  # validations
  validates :name, :collection_id, presence: true

  # enums
  enum condition: [:mint, :near_mint, :very_good, :good, :used, :damaged]

end
