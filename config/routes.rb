Rails.application.routes.draw do

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  resources :collections do
    member do
      post :public
      post :hide
    end
  end

  resources :items, except: [:index]
  resources :offers do
    member do
      post :accept
      post :reject
    end
  end

  resources :comments, only: [:create]
  resources :profiles, only: [:show]

  get 'home/index'

  devise_for :users
  root 'home#index'
end
