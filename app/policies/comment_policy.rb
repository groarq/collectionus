class CommentPolicy < ApplicationPolicy

  def create?
    record.commentable.seller == user || record.commentable.buyer == user
  end
end
