class CreateCollections < ActiveRecord::Migration
  def change
    create_table :collections do |t|

      t.references :user, index: true
      t.string :name
      t.string :tags
      t.string :contains
      t.string :image


      t.timestamps null: false
    end
  end
end
