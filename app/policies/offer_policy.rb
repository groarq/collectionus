class OfferPolicy < ApplicationPolicy

  def show?
    record_user?
  end

  def accept?
    @record.seller == @user
  end

  def reject?
    accept?
  end

  def record_user?
    @user == @record.seller || @user == @record.buyer
  end
end
